package net.dotefekts.intime;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class InTime extends JavaPlugin {

	Logger log = Logger.getLogger("Minecraft");
	public static Map<String, Boolean> timeFrozen = new HashMap<String, Boolean>();
	TimeFunctions tf = new TimeFunctions();
	ListenerMethods listener = new ListenerMethods();
	
	public void onEnable(){
		FileConfiguration file = this.getConfig();
		if(!Bukkit.getServer().getPluginManager().isPluginEnabled("PermissionsEx")){
			Logger.getLogger("Minecraft").warning("[InTime] PermissionsEx plugin wasn't found. Disabling plugin.");
			Bukkit.getServer().getPluginManager().disablePlugin(this);
		}
		if(!file.getKeys(true).contains("ResetConfig")||file.getBoolean("ResetConfig")==true){
			File configdel = new File(getDataFolder(), "config.yml");
			configdel.delete();
			this.reloadConfig();
			file=this.getConfig();
			file.set("ResetConfig", false);
			file.set("Enabled", true);
			file.set("KickMessage", "Sorry, you've run out of time. You can buy more at (Website)");
			file.set("Default", "");
			file.set("Default.Hours", 1);
			file.set("Default.Minutes", 0);
			file.set("Default.Seconds", 0);
			file.set("Users", "");
			file.set("Users.ratchet11110", "");
			file.set("Users.ratchet11110.Hours", 23);
			file.set("Users.ratchet11110.Minutes", 59);
			file.set("Users.ratchet11110.Seconds", 59);
			this.saveConfig();
			Logger.getLogger("Minecraft").info("[InTime] Configuration file reset.");
		}
		if(file.contains("Enabled")){
			if(!file.getBoolean("Enabled")){
				log.warning("[InTime] Configuration set to disable plugin.");
				log.warning("[InTime] Disabling.");
				Bukkit.getServer().getPluginManager().disablePlugin(this);
			}
		} else {
			file.set("Enabled", true);
		}
		listener.storeInstance(this);
		tf.storeInstance(this);
		SecondDeduction runnable = new SecondDeduction();
		runnable.storeInstance(this);
		getServer().getPluginManager().registerEvents(listener,this);
		getServer().getScheduler().scheduleSyncRepeatingTask(this, runnable, 20L, 20L);
		log.info("[InTime] Enabled!");
		Player[] player = Bukkit.getServer().getOnlinePlayers();
		PermissionManager permissions = PermissionsEx.getPermissionManager();
		for(int i=0;i<player.length;i++){
			if(permissions.has(player[i], "intime.time.freeze")){
				tf.setTimeFrozen(player[i].getDisplayName(), true);
			}
		}
	}
	
	public Map<String, Boolean> getHash(){
		return timeFrozen;
	}
	
	public void onDisable(){ 
	 
		log.info("[InTime] Disabling.");
		
	}
	
	public FileConfiguration getConfigFile(){
		FileConfiguration files = getConfig();
		return files;
	}
	
}