package net.dotefekts.intime;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class SecondDeduction implements java.lang.Runnable {

	TimeFunctions tf = null;
	InTime main = null;
	
	public void storeInstance(InTime o){
		main=o;
		tf=main.tf;
	}
	
	public void run(){
		Player[] player = Bukkit.getServer().getOnlinePlayers();
		for(int i=0;i<player.length;i++){
			int[] amount = {0,0,1};
			if(!tf.isTimeFrozen(player[i].getDisplayName())){
				tf.takeTime(player[i].getDisplayName(), amount);
				if(!tf.hasTime(player[i].getDisplayName())){
					amount[2] = 0;
					tf.setTime(player[i].getDisplayName(), amount);		
					player[i].kickPlayer(main.getConfig().getString("KickMessage"));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("01:00:00")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:30:00")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:15:00")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:10:00")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:05:00")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:02:30")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:02:00")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:01:30")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:01:00")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:45")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:30")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:15")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:10")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:09")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:08")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:07")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:06")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:05")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:04")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:03")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:02")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
				if(tf.checkTime(player[i].getDisplayName()).equals("00:00:01")){
					player[i].sendMessage(ChatColor.RED+"You have "+tf.checkTime(player[i].getDisplayName()));
				}
			}
		}
		
	}
	
}
