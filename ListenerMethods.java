package net.dotefekts.intime;

import java.util.HashMap;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.inventory.ItemStack;

import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class ListenerMethods implements Listener {
	
	Logger log = Logger.getLogger("Minecraft");
	TimeFunctions tf = null;
	InTime plugin = null;
	FileConfiguration file = null;
	
	public void storeInstance(InTime o){
		plugin = o;
		file = o.getConfig();
		tf = o.tf;
	}
	
	 @EventHandler
	 public void normalLogin(PlayerJoinEvent event) {
		 PermissionManager permissions = PermissionsEx.getPermissionManager();  
		 String playerJoined = event.getPlayer().getDisplayName();
		 if(!file.contains("Users."+playerJoined)){
			 int defhours=6,defmins=0,defsecs=0;
			 if(file.contains("Default.Hours")){
				 defhours=file.getInt("Default.Hours");
			 }
			 if(file.contains("Default.Minutes")){
				 defmins=file.getInt("Default.Minutes");
			 }
			 if(file.contains("Default.Seconds")){
				 defsecs=file.getInt("Default.Seconds");
			 }
			 file.set("Users."+playerJoined+".Hours", defhours);
			 file.set("Users."+playerJoined+".Minutes", defmins);
			 file.set("Users."+playerJoined+".Seconds", defsecs);
			 plugin.saveConfig();
			 System.out.println(defhours+":"+defmins+":"+defsecs);
		 }
	        if(!tf.hasTime(playerJoined)){
	        	if(!permissions.has(event.getPlayer(), "intime.time.bipass")){
	        		System.out.println("[InTime] "+event.getPlayer().getDisplayName()+" was kicked (Ran out of time)");
	        		event.getPlayer().kickPlayer(file.getString("KickMessage"));
	        	}
	        }
			if(permissions.has(event.getPlayer(), "intime.time.freeze")){
				 tf.setTimeFrozen(event.getPlayer().getDisplayName(), true);
			}
	    }
	
	 @EventHandler
	 public void onCommand(PlayerCommandPreprocessEvent event) {
		 PermissionManager permissions = PermissionsEx.getPermissionManager();
		 Player player = event.getPlayer();
		 String cmd = event.getMessage();
		 String[] command = cmd.split("\\s+");
		 boolean match=false;
		 if(command.length==3){
			 if(Bukkit.getServer().getPlayer(command[1])!=null){
				 command[1]=Bukkit.getServer().getPlayer(command[1]).getDisplayName();
				 match=true;
			 }
		 }
		 if(command[0].equalsIgnoreCase("/checkfrozen")){
			 if(command.length>2){
				 player.sendMessage(ChatColor.RED+"Invalid usage");
				 player.sendMessage(ChatColor.RED+"Correct usage is /checkfrozen <player>");
				 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
			 }
			 if(command.length==1){
				boolean trse = tf.isTimeFrozen(event.getPlayer().getDisplayName());
			 	if(trse){
				 	String trs= "Your time is frozen.";
				 	event.getPlayer().sendMessage(ChatColor.GREEN+trs);
			 	} else {
				 	String trs= "Your time is not frozen.";
				 	event.getPlayer().sendMessage(ChatColor.GREEN+trs);
			 	}
			 }
			 if(command.length==2){
				 if(Bukkit.getServer().getPlayer(command[1])!=null){
					 command[1]=Bukkit.getServer().getPlayer(command[1]).getDisplayName();
					 boolean trse = tf.isTimeFrozen(command[1]);
				 		if(trse){
				 			String trs= command[1]+"'s time is frozen.";
				 			event.getPlayer().sendMessage(ChatColor.GREEN+trs);
				 		} else {
				 			String trs= command[1]+"'s time is not frozen.";
				 			event.getPlayer().sendMessage(ChatColor.GREEN+trs);
				 		} 
				 }
			 }
		 }
		 if(command[0].equalsIgnoreCase("/checktime")){
			 if(permissions.has(player, "intime.time.check")){
					 if(command.length>2){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /checktime <player>");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==1){
						 player.sendMessage(ChatColor.GREEN+"You have "+tf.checkTime(player.getDisplayName()));
					 }
					 
					 if(command.length==2){
						 if(Bukkit.getServer().getPlayer(command[1])!=null){
							 command[1]=Bukkit.getServer().getPlayer(command[1]).getDisplayName();
							 if(permissions.has(player, "intime.time.check.other")){
								 player.sendMessage(ChatColor.GREEN+""+command[1]+"'s time is "+tf.checkTime(command[1]));
							 } else {
								 player.sendMessage(ChatColor.RED+"You don't have permission for that command.");
							 }
						 } else {
							 player.sendMessage(ChatColor.RED+"Invalid target.");
						 }
					 }
				} else {
					player.sendMessage(ChatColor.RED+"You don't have permission for that command.");
				}
		 }
		 if(command[0].equalsIgnoreCase("/settime")){
				 if(permissions.has(player, "intime.time.set")){
					 if(command.length>3){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /settime <player> [amount]");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==1){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /settime <player> [amount]");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==2){
						 String[] amountarray = command[1].split(":");
						 if(command[1].matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
							 int amount[] = {-1,-1,-1};
							 for(int i=0; i<amountarray.length;i++){
								 amount[i] = Integer.parseInt(amountarray[i]);
							 }
							 if(amount[2]==-1&&amount[1]==-1){
								 amount[2]=amount[0];
								 amount[1]=0;
								 amount[0]=0;
							 } if(amount[2]==-1&&amount[1]!=-1){
								 amount[2]=amount[1];
								 amount[1]=amount[0];
								 amount[0]=0;
							 } else {
							 }
							 	int[] amountparsed = tf.parseTimeArray(amount);
							 	tf.setTime(event.getPlayer().getDisplayName(), amountparsed);
							 	System.out.println("[InTime] "+player.getDisplayName()+" set their time to  "+tf.parseTimeString(command[1]));
								event.getPlayer().sendMessage(ChatColor.GREEN+"Your time was set to "+tf.parseTimeString(command[1]));
						 } else {
							 event.getPlayer().sendMessage(ChatColor.RED+"Invalid usage");
							 event.getPlayer().sendMessage(ChatColor.RED+"Must be in the format h:m:s");
						}
					 }
					 if(command.length==3){
						 	if(!match){
						 		System.out.println("[InTime] "+"Player not found.");
						 		return;
						 	}
						 String[] amountarray = command[2].split(":");
						 if(command[2].matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
							 int amount[] = {-1,-1,-1};
							 for(int i=0; i<amountarray.length;i++){
								 amount[i] = Integer.parseInt(amountarray[i]);
							 }
							 if(amount[2]==-1&&amount[1]==-1){
								 amount[2]=amount[0];
								 amount[1]=0;
								 amount[0]=0;
							 } if(amount[2]==-1&&amount[1]!=-1){
								 amount[2]=amount[1];
								 amount[1]=amount[0];
								 amount[0]=0;
							 } else {
							 }
							 	int[] amountparsed = tf.parseTimeArray(amount);
							 	tf.setTime(command[1], amountparsed);
							 	System.out.println("[InTime] "+player.getDisplayName()+" set "+command[1]+"'s time to "+tf.parseTimeString(command[2]));
								Bukkit.getServer().getPlayer(command[1]).sendMessage(ChatColor.GREEN+"Your time was set to "+tf.parseTimeString(command[2])+" by "+event.getPlayer().getDisplayName());
								player.sendMessage(ChatColor.GREEN+command[1]+"'s time was set to "+tf.parseTimeString(command[2]));
						 } else {
								System.out.println("[InTime] "+"Invalid usage");
								System.out.println("[InTime] "+"Must be in the format h:m:s");
						}
					}
				 } else {
					 player.sendMessage(ChatColor.RED+"You don't have permission for that command.");
				 }
		 } 
		 if(command[0].equalsIgnoreCase("/paytime")){
				 if(permissions.has(player, "intime.time.pay")){
					 if(command.length>3){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /paytime [player] [amount]");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==1){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /paytime [player] [amount]");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==2){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /paytime [player] [amount]");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==3){
						 if(!match){
					 		System.out.println("[InTime] "+"Player not found.");
					 		return;
					 	}
						if(Bukkit.getServer().getPlayer(command[1])!=null){
							Player playerpaid = Bukkit.getServer().getPlayer(command[1]);
							String playerpaying = event.getPlayer().getDisplayName();
							String[] amountarray = command[2].split(":");
							if(tf.hasTime(command[1])){
							 if(command[2].matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
								 int amount[] = {-1,-1,-1};
								 for(int i=0; i<amountarray.length;i++){
									 amount[i] = Integer.parseInt(amountarray[i]);
								 }
								 if(amount[2]==-1&&amount[1]==-1){
									 amount[2]=amount[0];
									 amount[1]=0;
									 amount[0]=0;
								 } if(amount[2]==-1&&amount[1]!=-1){
									 amount[2]=amount[1];
									 amount[1]=amount[0];
									 amount[0]=0;
								 } else {
								 }
								 int[] amountparsed = tf.parseTimeArray(amount);
								 if(tf.payTime(playerpaying, command[1], amountparsed)){ 
									 System.out.println("[InTime] "+player.getDisplayName()+" payed "+tf.parseTimeString(command[2])+" to "+command[1]);
								player.sendMessage(ChatColor.GREEN+""+tf.parseTimeString(command[2])+" paid to "+playerpaid.getDisplayName());
								playerpaid.sendMessage(ChatColor.GREEN+""+tf.parseTimeString(command[2])+" was paid to you by "+event.getPlayer().getDisplayName());
								} else {
									player.sendMessage(ChatColor.GREEN+"There was an error in the transaction.");
								}
							} else {
								player.sendMessage(ChatColor.RED+"Invalid usage");
								player.sendMessage(ChatColor.RED+"Must be in the format h:m:s");
							}
						} else {
							player.sendMessage(ChatColor.RED+"That player has no time left");
							player.sendMessage(ChatColor.RED+"You must resurrect them with /timein [player]");
						}
					 	} else {
					 		player.sendMessage(ChatColor.RED+"Invalid target");
						}
					 }
				 } else {
					 player.sendMessage(ChatColor.RED+"You don't have permission for that command.");
				 }
		 }
		 if(command[0].equalsIgnoreCase("/addtime")){
			 	if(permissions.has(player, "intime.time.add")){
					 if(command.length>3){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /addtime <player> [amount]");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==1){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /addtime <player> [amount]");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==2){
						 String[] amountarray = command[1].split(":");
						 if(command[1].matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
							 int amount[] = {-1,-1,-1};
							 for(int i=0; i<amountarray.length;i++){
								 amount[i] = Integer.parseInt(amountarray[i]);
							 }
							 if(amount[2]==-1&&amount[1]==-1){
								 amount[2]=amount[0];
								 amount[1]=0;
								 amount[0]=0;
							 } if(amount[2]==-1&&amount[1]!=-1){
								 amount[2]=amount[1];
								 amount[1]=amount[0];
								 amount[0]=0;
							 } else {
							 }
							 int[] amountparsed = tf.parseTimeArray(amount);
							 tf.giveTime(event.getPlayer().getDisplayName(), amountparsed); 
							 System.out.println("[InTime] "+player.getDisplayName()+" gave "+tf.parseTimeString(command[1])+" to themself");
							 player.sendMessage(ChatColor.GREEN+""+tf.parseTimeString(command[1])+" has been given to you.");
						 } else {
							 player.sendMessage(ChatColor.RED+"Invalid usage");
							 player.sendMessage(ChatColor.RED+"Must be in the format h:m:s");
						 }
					 }
					 if(command.length==3){
						 	if(!match){
						 		System.out.println("[InTime] "+"Player not found.");
						 		return;
						 	}
						 if(Bukkit.getServer().getPlayer(command[1])!=null){
							Player playerpaid = Bukkit.getServer().getPlayer(command[1]);
							String[] amountarray = command[2].split(":");
							 if(command[2].matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
								 int amount[] = {-1,-1,-1};
								 for(int i=0; i<amountarray.length;i++){
									 amount[i] = Integer.parseInt(amountarray[i]);
								 }
								 if(amount[2]==-1&&amount[1]==-1){
									 amount[2]=amount[0];
									 amount[1]=0;
									 amount[0]=0;
								 } if(amount[2]==-1&&amount[1]!=-1){
									 amount[2]=amount[1];
									 amount[1]=amount[0];
									 amount[0]=0;
								 } else {
								 }
								 int[] amountparsed = tf.parseTimeArray(amount);
								 	tf.giveTime(command[1], amountparsed);
								 	System.out.println("[InTime] "+player.getDisplayName()+" payed "+tf.parseTimeString(command[2])+" to "+command[1]);
									player.sendMessage(ChatColor.GREEN+""+tf.parseTimeString(command[2])+" given to "+playerpaid.getDisplayName());
									playerpaid.sendMessage(ChatColor.GREEN+""+tf.parseTimeString(command[2])+" was given to you by "+event.getPlayer().getDisplayName());
							} else {
								player.sendMessage(ChatColor.RED+"Invalid usage");
								player.sendMessage(ChatColor.RED+"Amount must be a number");
							}
						 } else {
							 player.sendMessage(ChatColor.RED+"Invalid target");
						 }
					 }
				 } else {
					 player.sendMessage(ChatColor.RED+"You don't have permission for that command.");
				 }
		 }
		 if(command[0].equalsIgnoreCase("/taketime")){
			 if(permissions.has(player, "intime.time.take")){
					 if(command.length>3){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /addtime <player> [amount]");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==1){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /addtime <player> [amount]");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==2){
						 String[] amountarray = command[1].split(":");
						 if(command[1].matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
							 int amount[] = {-1,-1,-1};
							 for(int i=0; i<amountarray.length;i++){
								 amount[i] = Integer.parseInt(amountarray[i]);
							 }
							 if(amount[2]==-1&&amount[1]==-1){
								 amount[2]=amount[0];
								 amount[1]=0;
								 amount[0]=0;
							 } if(amount[2]==-1&&amount[1]!=-1){
								 amount[2]=amount[1];
								 amount[1]=amount[0];
								 amount[0]=0;
							 } else {
							 }
							 int[] amountparsed = tf.parseTimeArray(amount);
							 tf.takeTime(event.getPlayer().getDisplayName(), amountparsed); 
							 System.out.println("[InTime] "+player.getDisplayName()+" took "+tf.parseTimeString(command[1])+" from themself");
							 player.sendMessage(ChatColor.GREEN+""+tf.parseTimeString(command[1])+" has been taken from you.");
						 } else {
							 player.sendMessage(ChatColor.RED+"Invalid usage");
							 player.sendMessage(ChatColor.RED+"Must be in the format h:m:s");
						 }
					 }
					 if(command.length==3){
						 	if(!match){
						 		System.out.println("[InTime] "+"Player not found.");
						 		return;
						 	}
						 if(Bukkit.getServer().getPlayer(command[1])!=null){
							Player playerpaid = Bukkit.getServer().getPlayer(command[1]);
							String[] amountarray = command[2].split(":");
							 if(command[2].matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
								 int amount[] = {-1,-1,-1};
								 for(int i=0; i<amountarray.length;i++){
									 amount[i] = Integer.parseInt(amountarray[i]);
								 }
								 if(amount[2]==-1&&amount[1]==-1){
									 amount[2]=amount[0];
									 amount[1]=0;
									 amount[0]=0;
								 } if(amount[2]==-1&&amount[1]!=-1){
									 amount[2]=amount[1];
									 amount[1]=amount[0];
									 amount[0]=0;
								 } else {
								 }
								 int[] amountparsed = tf.parseTimeArray(amount);
								 	tf.takeTime(command[1], amountparsed);
								 	System.out.println("[InTime] "+player.getDisplayName()+" took "+tf.parseTimeString(command[2])+" from "+command[1]);
									player.sendMessage(ChatColor.GREEN+""+tf.parseTimeString(command[2])+" taken from "+playerpaid.getDisplayName());
									playerpaid.sendMessage(ChatColor.GREEN+""+tf.parseTimeString(command[2])+" was taken from you by "+event.getPlayer().getDisplayName());
							} else {
								player.sendMessage(ChatColor.RED+"Invalid usage");
								player.sendMessage(ChatColor.RED+"Amount must be a number");
							}
						 } else {
							 player.sendMessage(ChatColor.RED+"Invalid target");
						 }
					 }
				 } else {
					 player.sendMessage(ChatColor.RED+"You don't have permission for that command.");
				 }
		 }
		 if(command[0].equalsIgnoreCase("/freezetime")){
			 if(permissions.has(player, "intime.time.freeze")){
					 if(command.length>2){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /freezetime <player>");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==1){
						 System.out.println("[InTime] "+player.getDisplayName()+"'s time was frozen");
						 if(tf.setTimeFrozen(event.getPlayer().getDisplayName())){ player.sendMessage(ChatColor.GREEN+"Your time has been frozen"); 
						 } else {
							 System.out.println("[InTime] "+player.getDisplayName()+"'s time was unfrozen");
							 player.sendMessage(ChatColor.GREEN+"Your time has been unfrozen");
						 }
					 }
					 if(command.length==2){
						 if(Bukkit.getServer().getPlayer(command[1])!=null){
							 command[1]=Bukkit.getServer().getPlayer(command[1]).getDisplayName();
								if(tf.setTimeFrozen(command[1])){ Bukkit.getServer().getPlayer(command[1]).sendMessage(ChatColor.GREEN+"Your time has been frozen");
								player.sendMessage(ChatColor.GREEN+""+command[1]+"'s time has been frozen");
								} else {
									Bukkit.getServer().getPlayer(command[1]).sendMessage(ChatColor.GREEN+"Your time has been unfrozen");
									player.sendMessage(ChatColor.GREEN+""+command[1]+"'s time has been unfrozen");
								}
						 } else {
							 player.sendMessage(ChatColor.RED+"Invalid target.");
						 }
					 }
				 } else {
					 player.sendMessage(ChatColor.RED+"You don't have permission for that command.");
				 }
		 }
		 
		 if(command[0].equalsIgnoreCase("/timein")){
			 if(permissions.has(player, "intime.time.timein")){
					 if(command.length>2){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /timein [player]");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==1){
						 player.sendMessage(ChatColor.RED+"Invalid usage");
						 player.sendMessage(ChatColor.RED+"Correct usage is /timein [player]");
						 player.sendMessage(ChatColor.RED+"[Required] <Optional>");
					 }
					 if(command.length==2){
						 int amount[] = {1,0,0};
						 int amountset[] = {0,30,0};
						 if(tf.hasEnough(player.getDisplayName(), amount)){
							 if(!tf.hasTime(command[1])&&plugin.getConfig().contains("Users."+command[1])){
								 tf.setTime(command[1], amountset);
								 player.sendMessage(ChatColor.GREEN+"Player timed in with 00:30:00");
								 player.sendMessage(ChatColor.RED+"1:00:00 deducted");
							 } else {
								 player.sendMessage(ChatColor.RED+"That player isn't timed out or doesn't exist."); 
							 }
						 } else {
							 player.sendMessage(ChatColor.RED+"Time in's cost one hour. You don't have enough.");
						 }
					 }
				 } else {
					 player.sendMessage(ChatColor.RED+"You don't have permission for that command.");
				 }
		 }
		 
	 }
	 
	 @EventHandler
	 public void onCommand(ServerCommandEvent event) {
		 String cmd = event.getCommand();
		 String[] command = cmd.split("\\s+");
		 boolean match=false;
		 if(command.length>1){
			 if(Bukkit.getServer().getPlayer(command[1])!=null){
				 command[1]=Bukkit.getServer().getPlayer(command[1]).getDisplayName();
				 match=true;
			 }
		 }
		 if(command[0].equalsIgnoreCase("addtime")){
			 	if(!match){
			 		System.out.println("[InTime] "+"Player not found.");
			 		return;
			 	}
				 if(command.length>3){
					 System.out.println("[InTime] "+"Invalid usage");
					 System.out.println("[InTime] "+"Correct usage is addtime [player] [amount]");
					 System.out.println("[InTime] "+"[Required] <Optional>");
				 }
				 if(command.length==1){
					 System.out.println("[InTime] "+"Invalid usage");
					 System.out.println("[InTime] "+"Correct usage is addtime [player] [amount]");
					 System.out.println("[InTime] "+"[Required] <Optional>");
				 }
				 if(command.length==2){
					 System.out.println("[InTime] "+"Invalid usage");
					 System.out.println("[InTime] "+"Correct usage is addtime [player] [amount]");
					 System.out.println("[InTime] "+"[Required] <Optional>");
				 }
				 if(command.length==3){
					 String[] amountarray = command[2].split(":");
					 if(command[2].matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
						 int amount[] = {-1,-1,-1};
						 for(int i=0; i<amountarray.length;i++){
							 amount[i] = Integer.parseInt(amountarray[i]);
						 }
						 if(amount[2]==-1&&amount[1]==-1){
							 amount[2]=amount[0];
							 amount[1]=0;
							 amount[0]=0;
						 } if(amount[2]==-1&&amount[1]!=-1){
							 amount[2]=amount[1];
							 amount[1]=amount[0];
							 amount[0]=0;
						 } else {
						 }
						 int[] amountparsed = tf.parseTimeArray(amount);
						 	tf.giveTime(command[1], amountparsed);
						 	System.out.println("[InTime] "+tf.parseTimeString(command[2])+" was given to "+command[1]);
							if(Bukkit.getServer().getPlayer(command[1]) != null){Bukkit.getServer().getPlayer(command[1]).sendMessage(ChatColor.GREEN+"You were given "+tf.parseTimeString(command[2])+" by the console");}
					} else {
						System.out.println("[InTime] "+"Invalid usage");
						System.out.println("[InTime] "+"Must be in the format h:m:s");
					}
				 }
		 }
		 
		 if(command[0].equalsIgnoreCase("taketime")){
			 	if(!match){
			 		System.out.println("[InTime] "+"Player not found.");
			 		return;
			 	}
			 if(command.length>3){
				 System.out.println("[InTime] "+"Invalid usage");
				 System.out.println("[InTime] "+"Correct usage is taketime [player] [amount]");
				 System.out.println("[InTime] "+"[Required] <Optional>");
			 }
			 if(command.length==1){
				 System.out.println("[InTime] "+"Invalid usage");
				 System.out.println("[InTime] "+"Correct usage is taketime [player] [amount]");
				 System.out.println("[InTime] "+"[Required] <Optional>");
			 }
			 if(command.length==2){
				 System.out.println("[InTime] "+"Invalid usage");
				 System.out.println("[InTime] "+"Correct usage is taketime [player] [amount]");
				 System.out.println("[InTime] "+"[Required] <Optional>");
			 }
			 if(command.length==3){
				 String[] amountarray = command[2].split(":");
				 if(command[2].matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
					 int amount[] = {-1,-1,-1};
					 for(int i=0; i<amountarray.length;i++){
						 amount[i] = Integer.parseInt(amountarray[i]);
					 }
					 if(amount[2]==-1&&amount[1]==-1){
						 amount[2]=amount[0];
						 amount[1]=0;
						 amount[0]=0;
					 } if(amount[2]==-1&&amount[1]!=-1){
						 amount[2]=amount[1];
						 amount[1]=amount[0];
						 amount[0]=0;
					 } else {
					 }
					 int[] amountparsed = tf.parseTimeArray(amount);
					 	tf.takeTime(command[1], amountparsed);
					 	System.out.println("[InTime] "+tf.parseTimeString(command[2])+" was taken from "+command[1]);
						if(Bukkit.getServer().getPlayer(command[1]) != null){Bukkit.getServer().getPlayer(command[1]).sendMessage(ChatColor.GREEN+""+tf.parseTimeString(command[2])+" was taken by the console");}
				} else {
					System.out.println("[InTime] "+"Invalid usage");
					System.out.println("[InTime] "+"Must be in the format h:m:s");
				}
			}
		 }
		 
		 if(command[0].equalsIgnoreCase("settime")){
			 	if(!match){
			 		System.out.println("[InTime] "+"Player not found.");
			 		return;
			 	}
			 if(command.length>3){
				 System.out.println("[InTime] "+"Invalid usage");
				 System.out.println("[InTime] "+"Correct usage is settime [player] [amount]");
				 System.out.println("[InTime] "+"[Required] <Optional>");
			 }
			 if(command.length==1){
				 System.out.println("[InTime] "+"Invalid usage");
				 System.out.println("[InTime] "+"Correct usage is settime [player] [amount]");
				 System.out.println("[InTime] "+"[Required] <Optional>");
			 }
			 if(command.length==2){
				 System.out.println("[InTime] "+"Invalid usage");
				 System.out.println("[InTime] "+"Correct usage is settime [player] [amount]");
				 System.out.println("[InTime] "+"[Required] <Optional>");
			 }
			 if(command.length==3){
				 String[] amountarray = command[2].split(":");
				 if(command[2].matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
					 int amount[] = {-1,-1,-1};
					 for(int i=0; i<amountarray.length;i++){
						 amount[i] = Integer.parseInt(amountarray[i]);
					 }
					 if(amount[2]==-1&&amount[1]==-1){
						 amount[2]=amount[0];
						 amount[1]=0;
						 amount[0]=0;
					 } if(amount[2]==-1&&amount[1]!=-1){
						 amount[2]=amount[1];
						 amount[1]=amount[0];
						 amount[0]=0;
					 } else {
					 }
					 int[] amountparsed = tf.parseTimeArray(amount);
					 	tf.setTime(command[1], amountparsed);
					 	System.out.println("[InTime] "+command[1]+"'s time was set to "+tf.parseTimeString(command[2]));
						if(Bukkit.getServer().getPlayer(command[1]) != null){Bukkit.getServer().getPlayer(command[1]).sendMessage(ChatColor.GREEN+"Your time was set to "+tf.parseTimeString(command[2])+" by the console");
					}
				 } else {
						System.out.println("[InTime] "+"Invalid usage");
						System.out.println("[InTime] "+"Must be in the format h:m:s");
				}
			 }
		 }
		 if(command[0].equalsIgnoreCase("checktime")){
			 	if(!match){
			 		System.out.println("[InTime] "+"Player not found.");
			 		return;
			 	}
			 if(command.length>2){
				 System.out.println("[InTime] "+"Invalid usage");
				 System.out.println("[InTime] "+"Correct usage is checktime [player]");
				 System.out.println("[InTime] "+"[Required] <Optional>");
			 }
			 if(command.length==1){
				 System.out.println("[InTime] "+"Invalid usage");
				 System.out.println("[InTime] "+"Correct usage is checktime [player]");
				 System.out.println("[InTime] "+"[Required] <Optional>");
			 }
			 if(command.length==2){
				 System.out.println("[InTime] "+command[1]+"'s balance is "+tf.checkTime(command[1]));
			 }
		 	}
		 if(command[0].equalsIgnoreCase("checkfrozen")){
			 if(command.length>2){
				 System.out.println("[InTime] "+"Invalid usage");
				 System.out.println("[InTime] "+"Correct usage is checkfrozen [player]");
				 System.out.println("[InTime] "+"[Required] <Optional>");
			 }
			 if(command.length==1){
				 System.out.println("[InTime] "+"Invalid usage");
				 System.out.println("[InTime] "+"Correct usage is checkfrozen [player]");
				 System.out.println("[InTime] "+"[Required] <Optional>");
			 }
			 if(command.length==2){
				 if(Bukkit.getServer().getPlayer(command[1])!=null){
					 command[1]=Bukkit.getServer().getPlayer(command[1]).getDisplayName();
					 boolean trse = tf.isTimeFrozen(command[1]);
				 		if(trse){
				 			String trs= command[1]+"'s time is frozen.";
				 			System.out.println("[InTime] "+trs);
				 		} else {
				 			String trs= command[1]+"'s time is not frozen.";
				 			System.out.println("[InTime] "+trs);
				 		} 
				 }
			 }
		 }
		 if(command[0].equalsIgnoreCase("freezetime")){
					 if(command.length>2){
						 System.out.println("[InTime] Invalid usage");
						 System.out.println("[InTime] Correct usage is freezetime [player]");
						 System.out.println("[InTime] [Required] <Optional>");
					 }
					 if(command.length==1){
						 System.out.println("[InTime] Invalid usage");
						 System.out.println("[InTime] Correct usage is freezetime [player]");
						 System.out.println("[InTime] [Required] <Optional>");
					 }
					 if(command.length==2){
						 if(Bukkit.getServer().getPlayer(command[1])!=null){
							 command[1]=Bukkit.getServer().getPlayer(command[1]).getDisplayName();
								if(tf.setTimeFrozen(command[1])){ Bukkit.getServer().getPlayer(command[1]).sendMessage(ChatColor.GREEN+"Your time has been frozen");
								System.out.println("[InTime] "+command[1]+"'s time has been frozen");
								} else {
									Bukkit.getServer().getPlayer(command[1]).sendMessage(ChatColor.GREEN+"Your time has been unfrozen");
									System.out.println("[InTime] "+command[1]+"'s time has been unfrozen");
								}
						 } else {
							 System.out.println("[InTime] "+"Invalid target.");
						 }
					 }
		 }
		 if(command[0].equalsIgnoreCase("timein")){
					 if(command.length>2){
						 System.out.println("[InTime] Invalid usage");
						 System.out.println("[InTime] Correct usage is timein [player]");
						 System.out.println("[InTime] [Required] <Optional>");
					 }
					 if(command.length==1){
						 System.out.println("[InTime] Invalid usage");
						 System.out.println("[InTime] Correct usage is timein [player]");
						 System.out.println("[InTime] [Required] <Optional>");
					 }
					 if(command.length==2){
						 int amountset[] = {0,30,0};
						 if(!tf.hasTime(command[1])&&plugin.getConfig().contains("Users."+command[1])){
							 tf.setTime(command[1], amountset);
							 System.out.println("[InTime] Player timed in with 00:30:00");
						 } else {
						 System.out.println("[InTime] That player isn't timed out or doesn't exist."); 
					}
				}
		 }
		 }
	 
	 @SuppressWarnings("deprecation")
	@EventHandler
	 public void signRightClick(PlayerInteractEvent event){
		 PermissionManager permissions = PermissionsEx.getPermissionManager();
		 Player player = event.getPlayer();
		 if(event.getAction()==Action.RIGHT_CLICK_AIR||event.getAction()==Action.RIGHT_CLICK_BLOCK){
			 if(event.hasItem()){
			 	if(event.getItem().getTypeId()==347){
				 	player.sendMessage(ChatColor.GREEN+"You have "+tf.checkTime(player.getDisplayName()));
			 	}
		 	}
		 }
		 if(event.hasBlock()){
			 Block block = event.getClickedBlock();
			 if(event.getAction()==Action.RIGHT_CLICK_BLOCK){
				 if(event.getClickedBlock().getTypeId()==63||event.getClickedBlock().getTypeId()==68){
					 Sign sign = (Sign) block.getState();
					 if(sign.getLine(1).equals("[TimeContainer]")){
						 if(sign.getLine(0).equalsIgnoreCase(event.getPlayer().getDisplayName())||sign.getLine(0).equals("Public")||permissions.has(event.getPlayer(), "intime.sign.container.override")){
							 if(permissions.has(event.getPlayer(), "intime.sign.container.use.give")){
							 String amountstring = sign.getLine(2);
							 String[] amountarray = amountstring.split(":");
							 if(amountstring.matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
								 int amount[] = {-1,-1,-1};
								 for(int i=0; i<amountarray.length;i++){
									 amount[i] = Integer.parseInt(amountarray[i]);
								 }
								 if(amount[2]==-1&&amount[1]==-1){
									 amount[2]=amount[0];
									 amount[1]=0;
									 amount[0]=0;
								 } if(amount[2]==-1&&amount[1]!=-1){
									 amount[2]=amount[1];
									 amount[1]=amount[0];
									 amount[0]=0;
								 } else {
								 }
								 amount = tf.parseTimeArray(amount);
								 if(tf.hasEnough(event.getPlayer().getDisplayName(), amount)){
								 	tf.takeTime(event.getPlayer().getDisplayName(), amount);
								 	String signamount = sign.getLine(3);
								 	if(signamount==""){
								 		signamount="00:00:00";
								 	}
								 	
								 		String parsedsign = tf.parseTimeRaw(tf.parseTime(signamount)+((amount[0]*60*60)+(amount[1]*60)+amount[2]));
								 		sign.setLine(3, parsedsign);
								 		sign.update();
								 	} else {
								 		event.getPlayer().sendMessage(ChatColor.GREEN+"You don't have enough to put into that sign.");
								 	}
								 	
							 } else {
								 event.getPlayer().sendMessage(ChatColor.GREEN+"Incorrect sign format");
								 event.getPlayer().sendMessage(ChatColor.GREEN+"Must be in the format h:m:s");							 
						 	}
							} else {
								event.getPlayer().sendMessage(ChatColor.RED+"You don't have permission to do that.");
							}
						 
						 } else {
							 event.getPlayer().sendMessage(ChatColor.RED+"You don't have permission for that sign.");
						 }
					 	event.setCancelled(true);
					 }
					 
					 if(sign.getLine(1).equals("[TimePower]")){
						  if(permissions.has(event.getPlayer(), "intime.sign.power.use")){
							 String amountstring = sign.getLine(2);
							 String[] amountarray = amountstring.split(":");
							 if(amountstring.matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
								 int amount[] = {-1,-1,-1};
								 for(int i=0; i<amountarray.length;i++){
									 amount[i] = Integer.parseInt(amountarray[i]);
								 }
								 if(amount[2]==-1&&amount[1]==-1){
									 amount[2]=amount[0];
									 amount[1]=0;
									 amount[0]=0;
								 } if(amount[2]==-1&&amount[1]!=-1){
									 amount[2]=amount[1];
									 amount[1]=amount[0];
									 amount[0]=0;
								 } else {
								 }
								 amount = tf.parseTimeArray(amount);
								 if(tf.hasEnough(event.getPlayer().getDisplayName(), amount)){
								 	tf.takeTime(event.getPlayer().getDisplayName(), amount);
								 	
								 	int blockid = 1;
								 	byte blockdata = 0x0;
								 	int id = block.getRelative(0, -1, 0).getTypeId();
								 	
								 	if(block.getData()==(byte)2){
								 		blockid=block.getRelative(0, -1, 1).getTypeId();
								 		if(blockid!=1){
								 			block.getRelative(0, -1, 1).setTypeId(1);
								 		} else {
								 			block.getRelative(0, -1, 1).setTypeId(2);
								 		}
								 		block.getRelative(0, -1, 0).setTypeIdAndData(77, (byte)4, true);
								 		block.getState().update();
								 		block.getRelative(0, -1, 0).setData((byte)12);
								 		block.getState().update();
								 		block.getRelative(0, -1, 0).setTypeId(0);
								 		block.getRelative(0, -1, 1).setTypeId(blockid);
								 		block.getRelative(0, -1, 1).setData(blockdata);
								 	} if(block.getData()==(byte)3){
								 		blockid=block.getRelative(0, -1, -1).getTypeId();
								 		if(blockid!=1){
								 			block.getRelative(0, -1, -1).setTypeId(1);
								 		} else {
								 		}
								 		block.getRelative(0, -1, 0).setTypeIdAndData(77, (byte)3, true);
								 		block.getState().update();
								 		block.getRelative(0, -1, 0).setData((byte)11);
								 		block.getState().update();
								 		block.getRelative(0, -1, 0).setTypeId(0);
								 		block.getRelative(0, -1, -1).setTypeId(blockid);
								 		block.getRelative(0, -1, -1).setData(blockdata);
								 	} if(block.getData()==(byte)4){
								 		blockid=block.getRelative(1, -1, 0).getTypeId();
								 		if(blockid!=1){
								 			block.getRelative(1, -1, 0).setTypeId(1);
								 		} else {
								 		}
								 		block.getRelative(0, -1, 0).setTypeIdAndData(77, (byte)2, true);
								 		block.getState().update();
								 		block.getRelative(0, -1, 0).setData((byte)10);
								 		block.getState().update();
								 		block.getRelative(0, -1, 0).setTypeId(0);
								 		block.getRelative(1, -1, 0).setTypeId(blockid);
								 		block.getRelative(1, -1, 0).setData(blockdata);
								 	} if(block.getData()==(byte)5){
								 		blockid=block.getRelative(-1, -1, 0).getTypeId();
								 		blockdata=block.getRelative(-1, -1, 0).getData();
								 		if(blockid!=1){
								 			block.getRelative(-1, -1, 0).setTypeId(1);
								 		} else {
								 		}
								 		block.getRelative(0, -1, 0).setTypeIdAndData(77, (byte)1, true);
								 		block.getState().update();
								 		block.getRelative(0, -1, 0).setData((byte)9);
								 		block.getState().update();
								 		block.getRelative(0, -1, 0).setTypeId(0);
								 		block.getRelative(-1, -1, 0).setTypeId(blockid);
								 		block.getRelative(-1, -1, 0).setData(blockdata);
								 		
								 	}
								 	block.getRelative(0, -1, 0).setTypeId(id);
								 } else {
								 	event.getPlayer().sendMessage(ChatColor.GREEN+"You don't have enough to do that.");
								 }
								 	
							} else {
								 event.getPlayer().sendMessage(ChatColor.GREEN+"Incorrect sign format");
								 event.getPlayer().sendMessage(ChatColor.GREEN+"Must be in the format h:m:s");							 
						 	}
						} else {
							event.getPlayer().sendMessage(ChatColor.RED+"You don't have permission to do that.");
						}
						  event.setCancelled(true);
					 }
					 if(sign.getLine(1).equals("[TimeShop]")){
						 if(permissions.has(player, "intime.sign.shop.sell")){	
							 if(sign.getLine(0).matches("^[0-9]{1,4}(:[0-9]{1,5})? [0-9]{1,9}$")&&sign.getLine(2).matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")&&sign.getLine(3).matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
								 String line1 = sign.getLine(0);
								 String itemamount = line1.split(" ")[1];
								 String[] line1a = line1.split(" ")[0].split(":");
								 int amount[] = {-1,-1,-1};
								 for(int i=0; i<line1a.length;i++){
									 amount[i] = Integer.parseInt(line1a[i]);
								 }
								 if(amount[1]==-1){
									 amount[1]= Integer.parseInt(itemamount);
								 } else {
									 amount[2]= Integer.parseInt(itemamount);
								 }
								 int itemid=0;
								 int itemamounti=0;
								 int itemdamage=0;
								 if(amount[0]!=-1){
									 itemid=amount[0];
								 }
								 if(amount[1]!=-1&&amount[2]!=-1){
									 itemdamage=amount[1];
									 itemamounti=amount[2];
								 } if(amount[1]!=-1&&amount[2]==-1){
									 itemamounti=amount[1];
								 }
								 
								 String amountstring = sign.getLine(3);
								 String[] amounttakenarray = amountstring.split(":");
								 int amounttaken[] = {-1,-1,-1};
								 for(int i=0; i<amounttakenarray.length;i++){
									 amounttaken[i] = Integer.parseInt(amounttakenarray[i]);
								 }
								 if(amounttaken[2]==-1&&amount[1]==-1){
									 amounttaken[2]=amount[0];
									 amounttaken[1]=0;
									 amounttaken[0]=0;
								 } if(amounttaken[2]==-1&&amount[1]!=-1){
									 amounttaken[2]=amount[1];
									 amounttaken[1]=amount[0];
									 amounttaken[0]=0;
								 } else {
								 }
								 
								 if(Material.getMaterial(itemid)!=null&&itemamounti!=0){
									
									 ItemStack item = new ItemStack(itemid, itemamounti, (short) itemdamage);
									
									 if(tf.hasItem(player.getInventory(), item)){
										 
										 tf.removeFromInventory(player.getInventory(), item);
										 player.updateInventory();
										 tf.giveTime(player.getDisplayName(), amounttaken);
										 player.sendMessage(ChatColor.GREEN+"You were given "+tf.parseTimeString(sign.getLine(3)));
										 
									 } else {
										 player.sendMessage(ChatColor.RED+"You don't have enough of that item to do that");
									 }
									 
								 } else {
									 player.sendMessage(ChatColor.RED+"Incorrect sign format");
									 player.sendMessage(ChatColor.RED+"Must be in the format item:(damage:)amount");
								 }
							} else {
								player.sendMessage(ChatColor.RED+"Incorrect sign format");
								player.sendMessage(ChatColor.RED+"Must be in the format h:mm:ss");
							}
						} else {
							player.sendMessage(ChatColor.RED+"You don't have permission to do that.");
						}
						 event.setCancelled(true);
					 } 
				 }
			 }
				 if(event.getAction()==Action.LEFT_CLICK_BLOCK){
					 if(event.getClickedBlock().getTypeId()==63||event.getClickedBlock().getTypeId()==68){
						 Sign sign = (Sign) block.getState();
						 if(sign.getLine(1).equals("[TimeContainer]")){
							 if(sign.getLine(0).equalsIgnoreCase(event.getPlayer().getDisplayName())||sign.getLine(0).equals("Public")||permissions.has(event.getPlayer(), "intime.sign.container.override")){
								 if(permissions.has(event.getPlayer(), "intime.sign.container.use.take")){
								 String amountstring = sign.getLine(2);
								 String[] amountarray = amountstring.split(":");
								 if(amountstring.matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
									 int amount[] = {-1,-1,-1};
									 for(int i=0; i<amountarray.length;i++){
										 amount[i] = Integer.parseInt(amountarray[i]);
									 }
									 if(amount[2]==-1&&amount[1]==-1){
										 amount[2]=amount[0];
										 amount[1]=0;
										 amount[0]=0;
									 } if(amount[2]==-1&&amount[1]!=-1){
										 amount[2]=amount[1];
										 amount[1]=amount[0];
										 amount[0]=0;
									 } else {
									 }
									 String signamount = sign.getLine(3);
									 if(signamount==""){
									 		signamount="00:00:00";
									 }
									 amount = tf.parseTimeArray(amount);
									 if(tf.parseTime(signamount)-((amount[0]*60*60)+(amount[1]*60)+amount[2])>-1){
										tf.giveTime(event.getPlayer().getDisplayName(), amount);
									 		
									 		String parsedsign = tf.parseTimeRaw(tf.parseTime(signamount)-((amount[0]*60*60)+(amount[1]*60)+amount[2]));
									 		sign.setLine(3, parsedsign);
									 		sign.update();
									 		
									 	} else {
									 		event.getPlayer().sendMessage(ChatColor.GREEN+"The sign doesn't have enought to give you.");
									 	}
									 	
								 } else {
									 event.getPlayer().sendMessage(ChatColor.RED+"Incorrect sign format");
									 event.getPlayer().sendMessage(ChatColor.RED+"Must be in the format h:m:s");							 
							 	}
								} else {
									event.getPlayer().sendMessage(ChatColor.RED+"You don't have permission to do that.");
								}
							 
							 } else {
								 event.getPlayer().sendMessage(ChatColor.RED+"You don't have permission for that sign.");
							 }
						 }
						 if(sign.getLine(1).equals("[TimeShop]")){
							 if(permissions.has(player, "intime.sign.shop.buy")){	
								 if(sign.getLine(0).matches("^[0-9]{1,4}(:[0-9]{1,5})? [0-9]{1,9}$")&&sign.getLine(2).matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")&&sign.getLine(3).matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
									 String line1 = sign.getLine(0);
									 String itemamount = line1.split(" ")[1];
									 String[] line1a = line1.split(" ")[0].split(":");
									 int amount[] = {-1,-1,-1};
									 for(int i=0; i<line1a.length;i++){
										 amount[i] = Integer.parseInt(line1a[i]);
									 }
									 if(amount[1]==-1){
										 amount[1]= Integer.parseInt(itemamount);
									 } else {
										 amount[2]= Integer.parseInt(itemamount);
									 }
									 int itemid=0;
									 int itemamounti=0;
									 int itemdamage=0;
									 if(amount[0]!=-1){
										 itemid=amount[0];
									 }
									 if(amount[1]!=-1&&amount[2]!=-1){
										 itemdamage=amount[1];
										 itemamounti=amount[2];
									 } if(amount[1]!=-1&&amount[2]==-1){
										 itemamounti=amount[1];
									 }
									 
									 String amountstring = sign.getLine(2);
									 String[] amounttakenarray = amountstring.split(":");
									 int amounttaken[] = {-1,-1,-1};
									 for(int i=0; i<amounttakenarray.length;i++){
										 amounttaken[i] = Integer.parseInt(amounttakenarray[i]);
									 }
									 if(amounttaken[2]==-1&&amounttaken[1]==-1){
										 amounttaken[2]=amounttaken[0];
										 amounttaken[1]=0;
										 amounttaken[0]=0;
									 } if(amounttaken[2]==-1&&amounttaken[1]!=-1){
										 amounttaken[2]=amounttaken[1];
										 amounttaken[1]=amounttaken[0];
										 amounttaken[0]=0;
									 } else {
									 }
									 
									 if(Material.getMaterial(itemid)!=null&&itemamounti!=0){
										
										 ItemStack item = new ItemStack(itemid, itemamounti, (short) itemdamage);
										
										 if(tf.hasEnough(player.getDisplayName(), amounttaken)){
											 
											 HashMap<Integer,ItemStack> leftover = player.getInventory().addItem(item);
											 tf.takeTime(player.getDisplayName(), amounttaken);
											 
											 if(!leftover.isEmpty()){
												 for(int i=0;i<leftover.size();i++){
													 ItemStack leftovertake = new ItemStack(itemid, itemamounti-leftover.get(i).getAmount(), (short) itemdamage);
													 tf.removeFromInventory(player.getInventory(), leftovertake);
													 tf.giveTime(player.getDisplayName(), amounttaken);
													 player.updateInventory();
												 }
												 player.sendMessage(ChatColor.RED+"Your inventory is too full");
											 } else {
												 player.sendMessage(ChatColor.GREEN+"You were given "+itemamount+" "+Material.getMaterial(itemid).name());
												 
											 }
											 
										 } else {
											 player.sendMessage(ChatColor.RED+"You don't have enough of that item to do that");
										 }
										 
									 } else {
										 player.sendMessage(ChatColor.RED+"Incorrect sign format");
										 player.sendMessage(ChatColor.RED+"Must be in the format item:(damage:)amount");
									 }
								} else {
									player.sendMessage(ChatColor.RED+"Incorrect sign format");
									player.sendMessage(ChatColor.RED+"Must be in the format h:mm:ss");
								}
							} else {
								player.sendMessage(ChatColor.RED+"You don't have permission to do that.");
							}
						 } 
					 }
			 	}
		 }
	 }
	 
	 @EventHandler
	 public void signChangeEvent(SignChangeEvent event){
		 PermissionManager permissions = PermissionsEx.getPermissionManager();
		 Block block = event.getBlock();
		 Player player = event.getPlayer();
		 if(event.getLine(1).equals("[TimeContainer]")){
			 if(permissions.has(player, "intime.sign.container.create")){
				 if(event.getLine(0).equals("")){
					 event.setLine(0, "Public");
				 } if (event.getLine(0).equalsIgnoreCase(player.getDisplayName())){
					 event.setLine(0, player.getDisplayName());
				 } if (permissions.has(player, "intime.sign.container.create.playeroverride")&&!event.getLine(0).equals(null)){
					 
				 } else {
					 block.breakNaturally();
					 player.sendMessage(ChatColor.RED+"You would not have control of that sign.");
				 }
				 if(event.getLine(2).matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
					 event.setLine(2, tf.parseTimeString(event.getLine(2)));
					 String signamount = event.getLine(3);
					 if(permissions.has(player, "intime.sign.container.create.timeoverride")){
							 if(signamount.matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
								 event.setLine(3, tf.parseTimeString(signamount));
							 } if(signamount.equals("")){
								 event.setLine(3, "00:00:00");
							 } if(!signamount.matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")) {
								 event.setLine(3, "00:00:00");
							 }
						 } else {
							 event.setLine(3, "00:00:00");
							 player.sendMessage(ChatColor.RED+"You don't have permission to create a container with time.");
						 }
					 player.sendMessage(ChatColor.GREEN+"Sign created successfully");
				 } else {
					 block.breakNaturally();
					 player.sendMessage(ChatColor.RED+"Incorrect sign format");
					 player.sendMessage(ChatColor.RED+"Must be in the format h:mm:ss");
				 }
			} else {
				 block.breakNaturally();
				 player.sendMessage(ChatColor.RED+"You don't have permission to do that.");
			}
		}
		 
		 if(event.getLine(1).equals("[TimePower]")){
			 if(permissions.has(player, "intime.sign.power.create")){
				 if(!event.getLine(0).equals("")){
					 event.setLine(0, "");
				 } 
				 
				 if(event.getLine(2).matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
					 if(tf.parseTimeString(event.getLine(2)).equals("00:00:00")){
						 if(permissions.has(player, "intime.sign.power.create.free")){ 
						 	} else {
							 block.breakNaturally();
							 player.sendMessage(ChatColor.RED+"You don't have permission to create a free sign.");
						 	}
					 	} else {
					 		event.setLine(2, tf.parseTimeString(event.getLine(2)));
						 if(!event.getLine(3).equals("")){
						 	event.setLine(3, "");
					 	}
					 	player.sendMessage(ChatColor.GREEN+"Sign created successfully");
					 }
				 } else {
					 block.breakNaturally();
					 player.sendMessage(ChatColor.RED+"Incorrect sign format");
					 player.sendMessage(ChatColor.RED+"Must be in the format h:mm:ss");
				 }
			} else {
				 block.breakNaturally();
				 player.sendMessage(ChatColor.RED+"You don't have permission to do that.");
			}
		}
		 
		 if(event.getLine(1).equals("[TimeShop]")){
			 if(permissions.has(player, "intime.sign.shop.create")){	
				 if(event.getLine(0).matches("^[0-9]{1,4}(:[0-9]{1,5})? [0-9]{1,9}$")&&event.getLine(2).matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")&&event.getLine(3).matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
					 String line1 = event.getLine(0);
					 String itemamount = line1.split(" ")[1];
					 String[] line1a = line1.split(" ")[0].split(":");
					 int amount[] = {-1,-1,-1};
					 for(int i=0; i<line1a.length;i++){
						 amount[i] = Integer.parseInt(line1a[i]);
					 }
					 if(amount[1]==-1){
						 amount[1]= Integer.parseInt(itemamount);
					 } else {
						 amount[2]= Integer.parseInt(itemamount);
					 }
					 int itemid=0;
					 int itemamounti=0;
					 if(amount[0]!=-1){
						 itemid=amount[0];
					 }
					 if(amount[1]!=-1&&amount[2]!=-1){
						 itemamounti=amount[2];
					 } if(amount[1]!=-1&&amount[2]==-1){
						 itemamounti=amount[1];
					 }
					 
					 if(Material.getMaterial(itemid)!=null&&itemamounti!=0){
						 if(tf.parseTimeString(event.getLine(2)).equals("00:00:00")){
							 if(permissions.has(player, "intime.sign.shop.create.free")){
							 } else {
						 		block.breakNaturally();
						 		player.sendMessage(ChatColor.RED+"You don't have permission to create a free sign.");
						 	 }
					 	}
						 event.setLine(2, tf.parseTimeString(event.getLine(2)));
						 event.setLine(3, tf.parseTimeString(event.getLine(3)));
						 
						 player.sendMessage(ChatColor.GREEN+"Sign created successfully");
					 } else {
						 block.breakNaturally();
						 player.sendMessage(ChatColor.RED+"Incorrect sign format");
						 player.sendMessage(ChatColor.RED+"Must be in the format item:(damage:)amount");
					 }
				} else {
					block.breakNaturally();
					player.sendMessage(ChatColor.RED+"Incorrect sign format");
					player.sendMessage(ChatColor.RED+"Must be in the format h:mm:ss");
				}
			} else {
				block.breakNaturally();
				player.sendMessage(ChatColor.RED+"You don't have permission to do that.");
			}
		 } 
	}
	 
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		PermissionManager permissions = PermissionsEx.getPermissionManager();
		 Block block = event.getBlock();
		 Player player = event.getPlayer();
		 if(block.getTypeId()==63||block.getTypeId()==68){
			 Sign sign = (Sign) block.getState();
			 if(sign.getLine(1).equals("[TimeContainer]")){
				 if((sign.getLine(0).equals("Public")||sign.getLine(0).equals(player.getDisplayName()))&&(permissions.has(player, "intime.sign.container.remove.public")||permissions.has(player, "intime.sign.container.remove.all"))){
					 String amountstring = sign.getLine(3);
					 String[] amountarray = amountstring.split(":");
					 int amount[] = {-1,-1,-1};
					 for(int i=0; i<amountarray.length;i++){
						 amount[i] = Integer.parseInt(amountarray[i]);
					 }
					 if(amount[2]==-1&&amount[1]==-1){
						 amount[2]=amount[0];
						 amount[1]=0;
						 amount[0]=0;
					} if(amount[2]==-1&&amount[1]!=-1){
						 amount[2]=amount[1];
						 amount[1]=amount[0];
						 amount[0]=0;
					} else {
					}
					tf.giveTime(player.getDisplayName(), amount);
					player.sendMessage(ChatColor.GREEN+"You were given "+tf.parseTimeRaw(tf.parseTimeToRaw(amount)));
				 } else if(!(sign.getLine(0).equals("Public")||sign.getLine(0).equals(player.getDisplayName()))&&permissions.has(player, "intime.sign.container.remove.all")){
					 String amountstring = sign.getLine(3);
					 String[] amountarray = amountstring.split(":");
					 int amount[] = {-1,-1,-1};
					 for(int i=0; i<amountarray.length;i++){
						 amount[i] = Integer.parseInt(amountarray[i]);
					 }
					 if(amount[2]==-1&&amount[1]==-1){
						 amount[2]=amount[0];
						 amount[1]=0;
						 amount[0]=0;
					} if(amount[2]==-1&&amount[1]!=-1){
						 amount[2]=amount[1];
						 amount[1]=amount[0];
						 amount[0]=0;
					} else {
					}
					tf.giveTime(player.getDisplayName(), amount);
					player.sendMessage(ChatColor.GREEN+"You were given "+amount[0]+":"+amount[1]+":"+amount[2]);
				 } else {
					 player.sendMessage(ChatColor.RED+"You don't have permission to do that");
					 event.setCancelled(true);
					 sign.update();
				 }
			 }
			 if(sign.getLine(1).equals("[TimePower]")){
				 if(!permissions.has(player, "intime.sign.power.remove")){
					event.setCancelled(true);
					player.sendMessage(ChatColor.RED+"You don't have permission to do that");
				 }
			 }
			 if(sign.getLine(1).equals("[TimeShop]")){
				 if(!permissions.has(player, "intime.sign.shop.remove")){
					event.setCancelled(true);
					player.sendMessage(ChatColor.RED+"You don't have permission to do that");
				 }
			 }
		 }
	}
}