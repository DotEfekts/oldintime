package net.dotefekts.intime;

import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class TimeFunctions {
	Logger log = Logger.getLogger("Minecraft");
	Map<String, Boolean> timeFrozen = InTime.timeFrozen;
	InTime plugin = null;
	FileConfiguration file = null;
	
	public void storeInstance(InTime o){
		plugin=o;
		file = o.getConfig();
	}
	
	public String checkTime(String player){
		
		int hours = file.getInt("Users."+player+".Hours");
		int minutes = file.getInt("Users."+player+".Minutes");
		int seconds = file.getInt("Users."+player+".Seconds");
		
		String hourss=null;
		String minutess=null;
		String secondss=null;
		
		if((int)Math.log10(Math.abs(hours)) + 1==1||hours==0){
			hourss="0"+hours;
		} else {
			hourss=""+hours;
		}
		if((int)Math.log10(Math.abs(minutes)) + 1==1||minutes==0){
			minutess="0"+minutes;
		} else {
			minutess=""+minutes;
		}
		if((int)Math.log10(Math.abs(seconds)) + 1==1||seconds==0){
			secondss="0"+seconds;
		} else {
			secondss=""+seconds;
		}
		
		String time = hourss+":"+minutess+":"+secondss;
		
		return time;
		
	}
	
	public boolean hasTime(String player) {
		
		int hours = file.getInt("Users."+player+".Hours");
		int minutes = file.getInt("Users."+player+".Minutes");
		int seconds = file.getInt("Users."+player+".Seconds");
		
		if(hours==0&&minutes==0&&seconds==0){
			return false;
		} else {
			return true;
		}
		
	}
	
	public void setTime(String player,int[] amountarray){
		
		file.set("Users."+player+".Hours", amountarray[0]);
		file.set("Users."+player+".Minutes", amountarray[1]);
		file.set("Users."+player+".Seconds", amountarray[2]);
		plugin.saveConfig();

	}
	
	public boolean payTime(String playerpaying, String playerpaid, int[] amount){
		
		if(hasEnough(playerpaying, amount)){
			
			takeTime(playerpaying, amount);
			giveTime(playerpaid, amount);
			
			return true;
		} else {
			return false;
		}
	}
	
	public void giveTime(String player, int[] amount){
		
		int hours = file.getInt("Users."+player+".Hours");
		int minutes = file.getInt("Users."+player+".Minutes");
		int seconds = file.getInt("Users."+player+".Seconds");
		
		int rawfetched = (hours*60*60)+(minutes*60)+seconds;
		int rawamount = (amount[0]*60*60)+(amount[1]*60)+amount[2];

		int rawtotal=rawfetched+rawamount;
		
		int totalhours = rawtotal / 3600,
			remainder = rawtotal % 3600,
			totalminutes = remainder / 60,
			totalseconds = remainder % 60;
		
		file.set("Users."+player+".Hours", totalhours);
		file.set("Users."+player+".Minutes", totalminutes);
		file.set("Users."+player+".Seconds", totalseconds);
		plugin.saveConfig();
		
	}
	
	public void takeTime(String player, int[] amount){
		
		int hours = file.getInt("Users."+player+".Hours");
		int minutes = file.getInt("Users."+player+".Minutes");
		int seconds = file.getInt("Users."+player+".Seconds");
		
		int rawfetched = (hours*60*60)+(minutes*60)+seconds;
		int rawamount = (amount[0]*60*60)+(amount[1]*60)+amount[2];

		int rawtotal=rawfetched-rawamount;
		
		if(rawtotal<0){
			rawtotal=0;
		}
		
		int totalhours = rawtotal / 3600,
			remainder = rawtotal % 3600,
			totalminutes = remainder / 60,
			totalseconds = remainder % 60;
		
		file.set("Users."+player+".Hours", totalhours);
		file.set("Users."+player+".Minutes", totalminutes);
		file.set("Users."+player+".Seconds", totalseconds);
		plugin.saveConfig();
		
	}
		
	public boolean hasEnough(String player, int[] amount){
		
		int hours = file.getInt("Users."+player+".Hours");
		int minutes = file.getInt("Users."+player+".Minutes");
		int seconds = file.getInt("Users."+player+".Seconds");
		
		int secondsleft = (hours*60*60)+(minutes*60)+seconds;
		int secondscheck = (amount[0]*60*60)+(amount[1]*60)+amount[2];
		
		if((secondsleft-secondscheck)>0){
			return true;
		} else {
			return false;
		}
		
	}
	
	public boolean isTimeFrozen(String player){
		PermissionManager permissions = PermissionsEx.getPermissionManager();
		if(Bukkit.getServer().getPlayer(player)==null){
			return false;
		} else if(permissions.has(Bukkit.getServer().getPlayer(player), "intime.freeze")){
			return true;
		} else if(timeFrozen.containsKey(player)){
			if(timeFrozen.get(player)){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public void setTimeFrozen(String player, boolean value){
		timeFrozen.put(player, value);
	}
	
	public boolean setTimeFrozen(String player){
		if(timeFrozen.containsKey(player)){
			if(timeFrozen.get(player)){
				timeFrozen.put(player, false);
				return false;
			} else {
				timeFrozen.put(player, true);
				return true;
			}
		} else {
			timeFrozen.put(player, true);
			return true;
		}
	}

	public void timeCheck(String player) {

		int hours = file.getInt("Users."+player+".Hours");
		int minutes = file.getInt("Users."+player+".Minutes");
		int seconds = file.getInt("Users."+player+".Seconds");
		
		if(hours<0||minutes<0||seconds<0){
			file.set("Users."+player+".Hours", 0);
			file.set("Users."+player+".Minutes", 0);
			file.set("Users."+player+".Seconds", 0);
			plugin.saveConfig();
		}
	}

	public int parseTime(String stringamount) {
		int rawamount = -1;
		if(stringamount.matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
		String[] amountarray = stringamount.split(":");
		int amount[] = {-1,-1,-1};
		for(int i=0; i<amountarray.length;i++){
			 amount[i] = Integer.parseInt(amountarray[i]);
		 }
		if(amount[2]==-1&&amount[1]==-1){
			 amount[2]=amount[0];
			 amount[1]=0;
			 amount[0]=0;
		 } if(amount[2]==-1&&amount[1]!=-1){
			 amount[2]=amount[1];
			 amount[1]=amount[0];
			 amount[0]=0;
		 } else {
		 }
		rawamount = (amount[0]*60*60)+(amount[1]*60)+amount[2];
		
		}
		return rawamount;
	}
	
	public String parseTimeRaw(int amount) {
		
		int hours = amount / 3600,
				remainder = amount % 3600,
				minutes = remainder / 60,
				seconds = remainder % 60;

			String hourss = "00";
			String minutess = "00";
			String secondss = "00";

		if((int)Math.log10(Math.abs(hours)) + 1==1||hours==0){
			
			hourss = "0"+hours;
			
		} else {
			
			hourss = ""+hours;
			
		}

		if((int)Math.log10(Math.abs(minutes)) + 1==1||minutes==0){
			
			minutess = "0"+minutes;
			
		} else {
			
			minutess = ""+minutes;
			
		}

		if((int)Math.log10(Math.abs(seconds)) + 1==1||seconds==0){
			
			secondss = "0"+seconds;
			
		} else {
			
			secondss = ""+seconds;
			
		}		


		String parsedstring=hourss+":"+minutess+":"+secondss;
		
		return parsedstring;
	}

	public String parseTimeString(String string) {
		
		int rawamount = 0;
		if(string.matches("^(([0-9]{1,9}:)?[0-9]{1,9}:)?[0-9]{1,9}$")){
			String[] amountarray = string.split(":");
			int amount[] = {-1,-1,-1};
			for(int i=0; i<amountarray.length;i++){
				amount[i] = Integer.parseInt(amountarray[i]);
			}
			if(amount[2]==-1&&amount[1]==-1){
				 amount[2]=amount[0];
				 amount[1]=0;
				 amount[0]=0;
			 } if(amount[2]==-1&&amount[1]!=-1){
				 amount[2]=amount[1];
				 amount[1]=amount[0];
				 amount[0]=0;
			 } else {
			 }
			rawamount = (amount[0]*60*60)+(amount[1]*60)+amount[2];
		
			int hours = rawamount / 3600,
				remainder = rawamount % 3600,
				minutes = remainder / 60,
				seconds = remainder % 60;

			String hourss = "00";
			String minutess = "00";
			String secondss = "00";

			if((int)Math.log10(Math.abs(hours)) + 1==1||hours==0){
			
				hourss = "0"+hours;
			
			} else {
			
				hourss = ""+hours;
			
			}

			if((int)Math.log10(Math.abs(minutes)) + 1==1||minutes==0){
			
				minutess = "0"+minutes;
			
			} else {
			
				minutess = ""+minutes;
			
			}

			if((int)Math.log10(Math.abs(seconds)) + 1==1||seconds==0){
			
				secondss = "0"+seconds;
			
			} else {
				
				secondss = ""+seconds;
			
			}		


			String parsedstring=hourss+":"+minutess+":"+secondss;
		
			return parsedstring;
		
		}
		
		return "00:00:00";
		
	}
	
	public int[] parseTimeArray(int[] amount) {
		
		int rawamount = (amount[0]*60*60)+(amount[1]*60)+amount[2];
		
			int hours = rawamount / 3600,
				remainder = rawamount % 3600,
				minutes = remainder / 60,
				seconds = remainder % 60;

			amount[0]=hours;
			amount[1]=minutes;
			amount[2]=seconds;
		
			return amount;
	}
	
public int parseTimeToRaw(int[] amount) {
		
		int rawamount = (amount[0]*60*60)+(amount[1]*60)+amount[2];
		
		return rawamount;
	}
	
	public boolean hasItem(Inventory inventory, ItemStack item) {
        int count = 0;
        ItemStack[] items = inventory.getContents();
        for (int i = 0; i < items.length; i++) {
            if (items[i] != null && items[i].getType() == item.getType() && items[i].getDurability() == item.getDurability()) {
                count += items[i].getAmount();
            }
            if (count >= item.getAmount()) {
                return true;
            }
        }
        return false;
    }
    
    public void removeFromInventory(Inventory inventory, ItemStack item) {
        int amt = item.getAmount();
        ItemStack[] items = inventory.getContents();
        for (int i = 0; i < items.length; i++) {
            if (items[i] != null && items[i].getType() == item.getType() && items[i].getDurability() == item.getDurability()) {
                if (items[i].getAmount() > amt) {
                    items[i].setAmount(items[i].getAmount() - amt);
                    break;
                } else if (items[i].getAmount() == amt) {
                    items[i] = null;
                    break;
                } else {
                    amt -= items[i].getAmount();
                    items[i] = null;
                }
            }
        }
        inventory.setContents(items);
    }
	
	
}